name := """sbt-ff-projects"""
organization := "com.fiftyforms"
version := "0.1.0-SNAPSHOT"

ThisBuild / semanticdbEnabled := true

console / initialCommands := """import com.fiftyforms.sbt._"""

enablePlugins(SbtPlugin)

scriptedLaunchOpts ++=
  Seq("-Xmx1024M", "-Dplugin.version=" + version.value)

addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.2")

addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.28")

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.5.1")

resolvers += Resolver.bintrayRepo("oyvindberg", "converter")

addSbtPlugin(
  "org.scalablytyped.converter" % "sbt-converter" % "1.0.0-beta32"
)

addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.20.0")

addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.0.6")
