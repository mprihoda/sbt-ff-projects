package com.fiftyforms.sbt

import lmcoursier.definitions.Authentication
import org.scalablytyped.converter.plugin.ScalablyTypedConverterPlugin
import org.scalablytyped.converter.plugin.ScalablyTypedPluginBase.autoImport._
import org.scalafmt.sbt.ScalafmtPlugin
import org.scalafmt.sbt.ScalafmtPlugin.autoImport._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt.Keys._
import sbt.{Def, _}
import scalafix.sbt.ScalafixPlugin
import scalafix.sbt.ScalafixPlugin.autoImport._
import scalajsbundler.sbtplugin.ScalaJSBundlerPlugin
import scalajsbundler.sbtplugin.ScalaJSBundlerPlugin.autoImport._
import com.gilcloud.sbt.gitlab.GitlabCredentials
import com.gilcloud.sbt.gitlab.GitlabPlugin
import com.gilcloud.sbt.gitlab.GitlabPlugin.autoImport._

object Versions {
  val webpack = "4.46.0"
  val webpackDevServer = "3.11.2"
  val scalajsReact = "1.7.7"
  val semanticUIReact = "2.0.3"
  val cats = "2.3.0"
  val react = "16.13.1"
  val monix = "3.4.0"
}

object FFScalaProjectPlugin extends AutoPlugin {
  override def requires: Plugins =
    ScalafixPlugin && ScalafmtPlugin && GitlabPlugin
  override def trigger: PluginTrigger = allRequirements

  object autoImport {
    val FFV = Versions
  }

  private val ffAuthCreds: Authentication = Authentication(
    Seq(("Private-Token", sys.env("FF_AUTH_TOKEN")))
  )

  override def projectSettings: Seq[Def.Setting[_]] = Seq(
    organization := "com.fiftyforms",
    resolvers += "ffAuth" at "https://gitlab.com/api/v4/groups/fifty-forms/-/packages/maven",
    csrConfiguration ~= (_.addRepositoryAuthentication("ffAuth", ffAuthCreds)),
    updateClassifiers / csrConfiguration ~= (_.addRepositoryAuthentication(
      "ffAuth",
      ffAuthCreds
    )),
    gitlabCredentials := {
      sys.env
        .get("FF_AUTH_TOKEN")
        .map(
          GitlabCredentials("Private-Token", _)
        ) orElse gitlabCredentials.value
    }
  )
}

object FFScalaJSScalablyTypedProjectPlugin extends AutoPlugin {
  override def requires: Plugins = ScalablyTypedConverterPlugin

  override def trigger: PluginTrigger = allRequirements

  override def globalSettings: Seq[Def.Setting[_]] = Seq(
    stRemoteCache := RemoteCache
      .S3(
        bucket = "ff-platform",
        pull = new java.net.URI(
          "https://ff-platform.ams3.digitaloceanspaces.com/scalablytyped"
        )
      )
      .withEndpoint("https://ams3.digitaloceanspaces.com")
      .withStaticCredentials(
        sys.env("FF_DO_ACCESS_KEY"),
        sys.env("FF_DO_SECRET_KEY")
      )
      .withRegion("ams3")
      .withPrefix("scalablytyped")
  )
}

object FFScalaJSBundlerProjectPlugin extends AutoPlugin {
  override def requires: Plugins = ScalaJSBundlerPlugin
  override def trigger: PluginTrigger = allRequirements

  override def projectSettings: Seq[Def.Setting[_]] = Seq(
    scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) },
    scalaJSUseMainModuleInitializer := true,
    webpackBundlingMode := BundlingMode.LibraryOnly(),
    webpackDevServerPort := 3000,
    webpack / version := Versions.webpack,
    startWebpackDevServer / version := Versions.webpackDevServer
  )
}

object FFCMIScalaProjectPlugin extends AutoPlugin {
  override def requires: Plugins = FFScalaProjectPlugin

  override def trigger: PluginTrigger = noTrigger

  override def projectSettings: Seq[Def.Setting[_]] = Seq(
    organization := "com.fiftyforms.cmi",
    publish / skip := true
  )
}
